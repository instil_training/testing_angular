
var Calc = function(a, b) {
    this.a = a;
    this.b = b;
    this.history = [];
    this.log = function(operand, result) {
        var log_msg = this.a + ' ' + operand + ' ' + this.b + ' = ' + result;
        this.history.unshift(log_msg);
    };
};

Calc.prototype.add = function() {
    var result = this.a + this.b;
    this.log('+', result);
    return result;
};

Calc.prototype.minus = function() {
    var result = this.a - this.b;
    this.log('-', result);
    return result;
};

Calc.prototype.multiply = function() {
    var result = this.a * this.b;
    this.log('*', result);
    return result;
};

Calc.prototype.divide = function() {
    if (this.b === 0) throw new Error('divide-by-zero');
    var result = this.a / this.b;
    this.log('/', result);
    return result;
};
