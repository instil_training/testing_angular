(function() {

    'use strict';

    describe('When using the calc module', function() {

        var calc;

        beforeEach(function() {
            calc = new Calc(4, 2);
        });

        it('should initialise with the provided values', function() {
            expect(calc).toBeDefined();
            expect(calc.a).toEqual(4);
            expect(calc.b).toEqual(2);
        });

        it('should provide a working add function', function() {
            expect(calc.add()).toEqual(6);
        });

        it('should provide a working minus function', function() {
            expect(calc.minus()).toEqual(2);
        });

        it('should provide a working multiply function', function() {
            expect(calc.multiply()).toEqual(8);
        });
        
        it('should provide a working divide function', function() {
            expect(calc.divide()).toEqual(2);
        });
        
        it('should throw an error if asked to divide by 0', function() {
            expect(function(){new Calc(4,0).divide();}).toThrow();
        });

        it('should log each calculation', function() {
            calc.add();
            expect(calc.history.length).toEqual(1);
            calc.minus();
            expect(calc.history.length).toEqual(2);
        });

        it('should log calculations in the proper format', function() {
            calc.add();
            expect(calc.history).toContain('4 + 2 = 6');
        });

    });

})();
