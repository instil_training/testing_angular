
var Calc = function(a, b) {
    this.a = a;
    this.b = b;
    this.history = [];
    this.log = function(operand, result) {
        var log_msg = this.a + ' ' + operand + ' ' + this.b + ' = ' + result;
        this.history.unshift(log_msg);
    };
};

Calc.prototype.add = function() {
    var result = this.a + this.b;
    this.log('+', result);
    return result;
};

// implement a minus function. make sure your function logs its results.

// implement a multiply function. make sure your function logs its results.

// implement a divide function. make sure your function logs its results.
// throw an error if the second parameter is 0

