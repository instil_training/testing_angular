(function() {

    'use strict';

    describe('The calc module', function() {

        var calc;

        // create a new instance of the Calc object
        // before every test

        it('should initialise the calc object properly', function() {
            // verify that calc is defined
            // verify the members of calc are set
        });

        it('should provide a working add function', function() {
            // verify that the add function works as expected
        });

        // add a test for a minus function.

        // add a test for a mulitply function.

        // add a test for a divide function.
        
        // add a test to check that the divide function throws an error
        // when passed 0

        // add a test to verify that calculations are being logged

        // add a test to verify that calculations are being logged in the proper format
        // e.g. "4 + 2 = 6"

    });

})();
