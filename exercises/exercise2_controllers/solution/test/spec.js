(function() {

    'use strict';

    describe('calc_controller', function() {

        var scope, controller;

        beforeEach(module('simpleCalc'));

        beforeEach(inject(function($controller, $rootScope) {
            scope = $rootScope.$new();
            controller = $controller('CalcController', {$scope:scope});
        }));

        it('should start in a known state', function() {
            expect(scope.firstNumber).toEqual(0);
            expect(scope.secondNumber).toEqual(0);
            expect(scope.result).toEqual(0);
        });

        it('should add by default', function() {
            scope.firstNumber = 12;
            scope.secondNumber = 23;
            controller.calculate();
            expect(scope.result).toEqual(35);
        });

        it('should export the correct operand symbol', function() {
            expect(scope.operand.symbol).toBe('+');
        });

        it('should use the chosen operand', function() {
            scope.operand = function(a,b){return 0;};
            controller.calculate();
            expect(scope.result).toEqual(0);
        });

        it('should throw an error if the arguments are not numbers', function() {
            scope.firstNumber = 'a';
            expect(controller.calculate).toThrow();
        });

    });

})();
