(function() {

    'use strict';

    var add = function(a,b){ return a+b; }; add.symbol = '+';
    var minus = function(a,b){ return a-b; }; minus.symbol = '-';
    var times = function(a,b){ return a*b; }; times.symbol = '*';
    var over = function(a,b){ return a/b; }; over.symbol = '/';

    angular.module('simpleCalc').controller('CalcController', function($scope) {
        this.calculate = function() {
            var a = parseInt($scope.firstNumber);
            var b = parseInt($scope.secondNumber);
            $scope.result = $scope.operand(a,b);
        };
        $scope.firstNumber = 0;
        $scope.secondNumber = 0;
        $scope.result = 0;
        $scope.operand = add;
        $scope.operands = [add, minus, times, over];
    });

})();
