(function() {

    'use strict';

    describe('calc_controller', function() {

        var scope, controller;

        // register the 'simpleCalc' module
        beforeEach();

        // create a new scope and inject it into a new controller instance
        beforeEach(inject(function($controller, $rootScope) {

        }));

        // verify that the firstNumber, secondNumber and result
        // attributes of the scope are all 0
        it('should start in a known state', function() {

        });

        // verify that the symbol attribute of the scope's operand is '+'
        it('should export the correct operand symbol', function() {

        });

        // verify that the controller will add the two numbers
        // when you call its 'calculate' function
        it('should add by default', function() {
            scope.firstNumber = 12;
            scope.secondNumber = 23;

        });

        // set the operand on the scope to a function of your
        // own and verify the results
        it('should use the chosen operand', function() {

        });

        // set one of the numbers to a letter and check that the calculate function
        // throws an error. if it doesn't, change the controller to throw an error.
        it('should throw an error if the arguments are not numbers', function() {

        });

    });

})();
