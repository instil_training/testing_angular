(function() {

    'use strict';

    var add = function(a,b){ return a+b; }; add.symbol = '+';
    var minus = function(a,b){ return a-b; }; minus.symbol = '-';
    var times = function(a,b){ return a*b; }; times.symbol = '*';
    var over = function(a,b){ return a/b; }; over.symbol = '/';

    angular.module('simpleCalc').controller('CalcController', function($scope, historyLogger) {
        $scope.firstNumber = 0;
        $scope.secondNumber = 0;
        $scope.result = 0;
        $scope.operand = add;
        $scope.operands = [add, minus, times, over];
        $scope.clearHistory = function() {
            $scope.history = historyLogger.clear();
        };
        this.calculate = function() {
            var a = parseInt($scope.firstNumber);
            var b = parseInt($scope.secondNumber);
            if (isNaN(a) || isNaN(b)) {
                throw "Both arguments must be numbers";
            }
            else {
                var result = $scope.operand(a,b);
                $scope.history = historyLogger.log(a, b, $scope.operand.symbol, result);
                $scope.result = result;
            }
        };
    });

})();
