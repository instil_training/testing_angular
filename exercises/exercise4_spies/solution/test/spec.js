(function() {
    
    'use strict';

    var scope, controller, historyLogger;
    var historyLoggerSpy;

    describe('When a calculation has been made', function() {

        beforeEach(module('simpleCalc'));

        beforeEach(inject(function($controller, $rootScope, _historyLogger_) {
            scope = $rootScope.$new();
            historyLogger = _historyLogger_;
            historyLoggerSpy = spyOn(historyLogger, 'log').and.callThrough();
            controller = $controller('CalcController', {$scope: scope, historyLogger: historyLogger});
        }));

        beforeEach(function() {
            historyLoggerSpy.calls.reset();
            scope.firstNumber = 1;
            scope.secondNumber = 2;
        });

        it('should log the result', function() {
            controller.calculate();
            expect(historyLoggerSpy).toHaveBeenCalled();
        });

        it('should log the result once for every calculation', function() {
            controller.calculate();
            controller.calculate();
            controller.calculate();
            expect(historyLoggerSpy.calls.count()).toEqual(3);
        });

        it('should log the expected message', function() {
            controller.calculate();
            expect(historyLoggerSpy).toHaveBeenCalledWith(1,2,'+',3);
        });
    
    });

})();
