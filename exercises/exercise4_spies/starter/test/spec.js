(function() {
    
    'use strict';

    var scope, controller, historyLogger;
    var historyLoggerSpy;

    describe('When a calculation has been made', function() {

        beforeEach(module('simpleCalc'));

        beforeEach(inject(function($controller, $rootScope, _historyLogger_) {
            scope = $rootScope.$new();
            historyLogger = _historyLogger_;
            // create a spy on the historyLogger and configure it to
            // call through to its original implementation
            historyLoggerSpy = ...;
            controller = $controller('CalcController', {$scope: scope, historyLogger: historyLogger});
        }));

        beforeEach(function() {
            scope.firstNumber = 1;
            scope.secondNumber = 2;
            // don't forget to reset the history logger spy
        });

        it('should log the result', function() {
            // call the controller calculate function and verify that the 
            // history logger was called
        });

        it('should log the result once for every calculation', function() {
            // call the controller calculate function 3 times 
            // and then verify that the history logger was called 3 times
        });

        it('should log the expected message', function() {
            // call the controller calculate function and
            // verify that the hsitory logger was called with the 
            // correct arguments
        });
    
    });

})();
