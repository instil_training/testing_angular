(function() {

    'use strict';

    angular.module('simpleCalc').factory('historyLogger', function() {
        var history = [];
        return {
            log: function(a, b, s, r) {
                    var msg = a + ' ' + s + ' ' + b + ' = ' + r;
                    history.unshift(msg);
                    return history;
                },
            clear: function() {
                history = [];
                return history;
            }
        };
    });

})();
