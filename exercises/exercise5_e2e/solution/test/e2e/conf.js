
exports.config = {
    
    seleniumAddress: 'http://localhost:4444/wd/hub',

    suites: {
        homepage: 'spec.js'
    },

    capabilities: {
      browserName: 'chrome'
    },

    framework: 'jasmine2',

}
