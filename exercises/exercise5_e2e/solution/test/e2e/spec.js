(function() {
    
    'use strict';

    describe('Simple Calculator application', function() {

        var firstNumber = element(by.model('firstNumber'));
        var secondNumber = element(by.model('secondNumber'));
        var result = element(by.binding('result'));
        var goButton = element(by.id('goButton'));
        var history = element.all(by.repeater('item in history'));

        beforeEach(function() {
            browser.get('http://localhost:8080');
        });

        it('should have a title', function() {
            expect(browser.getTitle()).toEqual('Simple Calculator');
        });

        it('should add numbers', function() {
            firstNumber.sendKeys(1);
            secondNumber.sendKeys(2);
            goButton.click();
            expect(result.getText()).toEqual('3');
        });

        it('should use the selected operand', function() {
            firstNumber.sendKeys(2);
            secondNumber.sendKeys(3);
            element(by.cssContainingText('option', '*')).click();
            goButton.click();
            expect(result.getText()).toEqual('6');
        });

        it('should display the result history', function() {
            firstNumber.sendKeys(2);
            secondNumber.sendKeys(3);
            goButton.click();
            expect(history.count()).toBe(1);
        });

    });

})();
