(function() {
    
    'use strict';

    describe('Simple Calculator application', function() {

        var firstNumber = // find the firstNumber element by its model
        var secondNumber = // find the secondNumber element but its model
        var result = // find the result element by its binding 
        var goButton = // find the goButton element by its id
        var history = // find the history element by its repeater

        beforeEach(function() {
            // use the browser object to navigate to your app
        });

        it('should have a title', function() {
            // verify that the page title is 'Simple Calculator'
            // note: the browser object has a getTitle() function
        });

        it('should add numbers', function() {
            // enter the value 1 into the firstNumber text entry

            // enter the value 2 into the secondNumber text entry

            // click the goButton

            // verify that the result element displays '3'
        });

        it('should use the selected operand', function() {
            // enter the value 2 into the firstNumber text entry
            
            // enter the value 3 into the secondNumber text entry
            
            // select the '*' operand from the dropdown
            // note: you will need to find the option by its css text value
            // and click it
            
            // click the goButton
            
            // verify that the result displays the value '6'
            
        });

        it('should display the result history', function() {
            // enter any value into the firstNumber text entry
            
            // enter any value into the secondNumber text entry
            
            // click the goButton
            
            // verify that the history list has one element
            // note: the history element has a count() function
        });

    });

})();
