(function() {
    
    'use strict';

    describe('Simple Calculator application', function() {

        var SimpleCalcPage = function() {
            this.firstNumber = element(by.model('firstNumber'));
            this.secondNumber = element(by.model('secondNumber'));
            this.result = element(by.binding('result'));
            this.goButton = element(by.id('goButton'));
            this.history = element.all(by.repeater('item in history'));
            
            this.start = function() {
                browser.get('http://localhost:8080');
            };

            function get_option_for_symbol(s) {
                return element(by.cssContainingText('option', s));
            }

            this.doCalculationWith = function(a, b, s) {
                this.firstNumber.sendKeys(a);
                this.secondNumber.sendKeys(b);
                if (s) { get_option_for_symbol(s).click(); }
                this.goButton.click();
            };

            this.lastResult = function() {
                return this.result.getText();
            };

            this.historySize = function() {
                return this.history.count();
            };
        };

        var page = new SimpleCalcPage();

        beforeEach(function() {
            page.start();
        });

        it('should have a title', function() {
            expect(browser.getTitle()).toEqual('Simple Calculator');
        });

        it('should add numbers', function() {
            page.doCalculationWith(1, 2);
            expect(page.lastResult()).toEqual('3');
        });

        it('should use the selected operand', function() {
            page.doCalculationWith(2, 3, '*');
            expect(page.lastResult()).toEqual('6');
        });

        it('should display the result history', function() {
            page.doCalculationWith(1, 2);
            expect(page.historySize()).toBe(1);
        });

    });

})();
