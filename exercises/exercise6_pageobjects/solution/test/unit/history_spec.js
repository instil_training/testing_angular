(function() {

    'use strict';

    describe('When using the history logger service', function() {

        var historyLogger;

        beforeEach(module('simpleCalc'));

        beforeEach(inject(function(_historyLogger_) {
            historyLogger = _historyLogger_;
        }));

        it('should add a new message to its history', function() {
            var history = historyLogger.log(1, 2, '+', 3);
            expect(history.length).toEqual(1);
        });

        it('should clear its history', function() {
            historyLogger.log(1, 2, '+', 3);
            var history = historyLogger.clear();
            expect(history.length).toEqual(0);
        });
        
    });

})();
