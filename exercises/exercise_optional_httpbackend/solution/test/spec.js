(function() {

    'use strict';

    describe('Fetching courses', function() {

        var scope, httpMock;
        var createController;

        var _baseURL = 'test.com/';

        var course = {id:1};
        var courses = [course];

        beforeEach(module('restApp'));

        beforeEach(inject(function($rootScope, $controller, $httpBackend) {
            scope = $rootScope.$new();
            httpMock = $httpBackend;
            httpMock.whenGET(_baseURL).respond(200, courses);
            createController = function() {
                return $controller('CoursesController', {baseURL: _baseURL, $scope: scope});
            };
        }));

        afterEach(function() {
            httpMock.verifyNoOutstandingExpectation();
            httpMock.verifyNoOutstandingRequest();
        });

        it('should request the course list on load', function() {
            httpMock.expectGET(_baseURL);
            createController();
            httpMock.flush();
            expect(scope.courses).toEqual(courses);
        });

        it('should clone the course being updated', function() {
            var controller = createController();
            httpMock.flush();
            controller.beginUpdateOfCourse(course);
            expect(scope.courseBeingUpdated).toBeDefined();
            expect(scope.courseBeingUpdated).not.toBe(course);
            expect(scope.courseBeingUpdated).toEqual(course);
        });

        it('should put an updated course to the server', function() {
            httpMock.whenPUT(_baseURL + course.id, course).respond(200, '');
            httpMock.expectPUT(_baseURL + course.id);
            httpMock.expectGET(_baseURL);
            var controller = createController();
            scope.courseBeingUpdated = course;
            controller.updateCourse();
            httpMock.flush();
        });

        it('should set the selected course on the scope', function() {
            var controller = createController();
            httpMock.flush();
            controller.selectCourse(course);
            expect(scope.noCourseSelected).toBe(false);
            expect(scope.selectedCourse).toBe(course);
        });

        it('should delete the requested course', function() {
            httpMock.whenDELETE(_baseURL + course.id).respond(200, '');
            httpMock.expectDELETE(_baseURL + course.id);
            httpMock.expectGET(_baseURL);
            var controller = createController();
            controller.deleteCourse(course);
            httpMock.flush();
        });

    });

})();
