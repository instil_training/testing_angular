var restApp = angular.module('restApp', []);

restApp.constant('baseURL','/rest/courses/');

restApp.controller('CoursesController', function (baseURL, $scope,$http) {
    $scope.noCourseSelected = true;
    $scope.courseBeingUpdated = false;
    
    this.fetchCourses = function() {
        function storeCourses(data) {
            $scope.courses = data;
        }
        $http.get(baseURL).success(storeCourses);
    };
    this.fetchCourses();
    
    this.beginUpdateOfCourse = function(course) {
        function cloneCourse(original) {
            var clone = {};
            for (var p in original) {
                if (original.hasOwnProperty(p)) {
                    clone[p] = original[p];
                }
            }
            return clone;
        }
        $scope.courseBeingUpdated = cloneCourse(course);
    };

    this.updateCourse = function() {
        var that = this;
        function resetForm() {
            $scope.courseBeingUpdated = false;
            that.fetchCourses();
        }
        var course = $scope.courseBeingUpdated;
        $http.put(baseURL + course.id, course).success(resetForm);
    };

    this.selectCourse = function(course) {
        $scope.noCourseSelected = false;
        $scope.selectedCourse = course;
    };

    this.deleteCourse = function(course) {
      $http.delete(baseURL + course.id ); 
      $scope.noCourseSelected = true;
      this.fetchCourses();
    };
});
