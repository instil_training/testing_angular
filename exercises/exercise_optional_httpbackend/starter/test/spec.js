(function() {

    'use strict';

    describe('Fetching courses', function() {

        var scope, httpMock;
        var createController;

        var baseUrl = 'test.com';

        var course = {};
        var courses = [course];

        beforeEach(module('restApp'));

        beforeEach(inject(function($rootScope, $controller, $httpBackend) {
            scope = $rootScope.$new();
            httpMock = $httpBackend;
            // setup mock behaviours on the httpMock object

            createController = function() {
                return $controller('CoursesController', {baseURL: baseUrl, $scope: scope});
            };
        }));

        afterEach(function() {
            httpMock.verifyNoOutstandingExpectation();
            httpMock.verifyNoOutstandingRequest();
        });

        it('should request the course list on load', function() {
            // implement this test
        });

        // test the other public methods on the CoursesController object

    });

})();
